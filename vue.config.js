const path = require('path')
const resolve = dir => {
	return path.join(__dirname, dir)
}
module.exports = {
	lintOnSave: false,
	publicPath: process.env.NODE_ENV === "production" ? "/" : "/",
	devServer: {
		port: 81,
		proxy: {
			'/api1': {
				target: process.env.VUE_APP_URL1,
				ws: false,
				changeOrigin: true,
				pathRewrite: {
					'^/api1': '' // 需要rewrite的,
				}
			},
			//VUE_APP_API_BASE_URL=http://175.27.129.169:5566
			'/api2': {
				target: process.env.VUE_APP_URL2,
				changeOrigin: true,
				pathRewrite: {
					'^/api2': '' // 需要rewrite的,
				}
			},
			'/hubs': {
				target: process.env.VUE_APP_SOCKET_BASE_URL,
				ws: true,
				changeOrigin: true
			}
		}
	},
	chainWebpack: config => {
		config.resolve.alias
			.set('_c', resolve('src/components')) // key,value自行定义，比如.set('@@', resolve('src/components'))
	}
}
