import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		token: '',
		companyList: [],
		companyId: 'cc01db43-1e21-4a2f-940b-cb446d01311b',
		lunboList: []
	},
	mutations: {
		SET_TOKEN: (state, token) => {
			state.token = token
		},
		SET_COMPANYLIST: (state, companyList) => {
			state.companyList = companyList
		},
		//SET_COMPANY_ID
		SET_COMPANY_ID: (state, companyId) => {
			state.companyId = companyId
		},
		SET_LUNBO_LIST: (state, lunboList) => {
			state.lunboList = lunboList
		},


	},
	actions: {},
	modules: {}
})
