const VueAxios = {
	vm: {},
	// eslint-disable-next-line no-unused-vars
	install(Vue, instance) {
		// console.log("installed", this.installed);
		if (this.installed) {
			return
		}
		this.installed = true

		if (!instance) {
			// eslint-disable-next-line no-console
			console.error('You have to install axios')
			return
		}
		// console.log("Vue.axios", Vue.axios)
		Vue.axios = instance

		Object.defineProperties(Vue.prototype, {
			axios: {
				get: function get() {
					return instance
				}
			},
			$http: {
				get: function get() {
					return instance
				}
			}
		})

		console.log("Vue.prototype", Vue.prototype);


	}
}

export {
	VueAxios
}
