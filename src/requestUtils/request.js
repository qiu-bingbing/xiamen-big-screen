import Vue from 'vue'
import axios from 'axios'
import store from '../store'
// import router from './router'
// import {
//   message,
//   Modal,
//   notification
// } from 'ant-design-vue' /// es/notification
import {
	VueAxios
} from './axios'
// import {
//   ACCESS_TOKEN
// } from '@/store/mutation-types'

// 创建 axios 实例
const service = axios.create({
	timeout: 100000, // 请求超时时间
})

console.log(23, process.env.VUE_APP_API_BASE_URL);


//本机生产环境不用跨域
if (process.env.NODE_ENV === 'production') {
	console.log("bbbb", process.env);
	// service.defaults.baseURL = process.env.VUE_APP_URL2;
	// service.defaults.baseURL = "/api2";
	// service.defaults.baseURL = "http://175.27.130.183:8000";
	// service.defaults.baseURL = "/api2";
	service.defaults.baseURL = process.env.VUE_APP_URL2;
	// service.defaults.baseURL = "/api2";
} else {
	console.log("bbbb11", process.env);
	service.defaults.baseURL = "/api1";
	// service.defaults.baseURL = "http://175.27.130.183:8000";
}


service.defaults.withCredentials = false;

// console.log(28, service);
const err = error => {
	// if (error.response) {
	//   const data = error.response.data
	//   const token = Vue.ls.get(ACCESS_TOKEN)
	//   if (error.response.status === 403) {
	//     console.log('服务器403啦，要重新登录！')
	//     notification.error({
	//       message: 'Forbidden',
	//       description: data.message
	//     })
	//   }
	//   if (error.response.status === 500) {
	//     if (data.message.length > 0) {
	//       message.error(data.message)
	//     }
	//   }
	//   if (error.response.status === 401 && !(data.result && data.result.isLogin)) {
	//     notification.error({
	//       message: 'Unauthorized',
	//       description: 'Authorization verification failed'
	//     })
	//     if (token) {
	//       store.dispatch('Logout').then(() => {
	//         setTimeout(() => {
	//           window.location.reload()
	//         }, 1500)
	//       })
	//     }
	//   }
	// }
	return Promise.reject(error)
}

// request interceptor
service.interceptors.request.use(config => {
	// console.log("请求拦截器", config);
	const token = store.state.token;
	// const refreshToken = Vue.ls.get('X-Access-Token')

	if (token) {
		config.headers['Authorization'] = 'Bearer ' + token
	}
	// if (refreshToken) {
	//   config.headers['X-Authorization'] = 'Bearer ' + refreshToken
	// }
	return config
}, err)

/**
 * response interceptor
 * 所有请求统一返回
 */
service.interceptors.response.use(response => {

	// response.headers["CorsAccessorSettings"] = {
	// 	"WithExposedHeaders": ["access-token", "x-access-token"]
	// }

	// console.log(68, response);
	// LocalStorage 存储的 token 和 refreshToken，不设定过期时间，由服务端统一处理
	if (response.headers['access-token'] && response.headers['access-token'] !== 'invalid_token') {
		localStorage.setItem("ACCESS_TOKEN", response.headers['access-token'] /*, 7 * 24 * 60 * 60 * 1000 */ )
		//保存token到vuex
		store.commit('SET_TOKEN', response.headers['access-token'])
		console.log(store);
	}
	if (response.headers['x-access-token']) {
		// Vue.ls.set('X-Access-Token', response.headers['x-access-token'] /*, 7 * 24 * 60 * 60 * 1000 */ )
	}
	if (response.request.responseType === 'blob') {
		return response
	}
	const resData = response.data
	const code = response.data.code
	// if (!store.state.app.hasError) {
	//   if (code ===
	//     401 /* || code === 1011006 || code === 1011007 || code === 1011008 || code === 1011009 */ ) {
	//     // Modal.error({
	//     //   title: '提示：',
	//     //   content: resData.message,
	//     //   keyboard: false,
	//     //   okText: '重新登录',
	//     //   onOk: () => {
	//     //     store.dispatch('SetHasError', false)
	//     //     window.location.reload()
	//     //   }
	//     // })

	//     // 授权过期，清理本地缓存的记录，不论 Modal.error 的 onOk 是否确认，先清理
	//     // 否则会在没按 OK 时，刷新网页或者重新访问，都会弹出“未授权的提示框”
	//     // 这样的调整后，TOKEN 为空直接重定向，SetHasError 的设置和判断其实已经用不上
	//     // Vue.ls.remove(ACCESS_TOKEN)
	//     // Vue.ls.remove('X-Access-Token')
	//     // store.dispatch('SetHasError', true)
	//   }
	//   // if (code === 1013002 || code === 1016002 || code === 1015002) {
	//   //   message.error(response.data.message)
	//   //   return response.data
	//   // }
	// }
	return resData
}, err)

const installer = {
	vm: {},
	install(Vue) {
		// console.log(12211111111);
		Vue.use(VueAxios, service)
	}
}

// console.log(service);
export {
	installer as VueAxios, service as axios
}