import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
	// {
	// 	path: '/',
	// 	name: 'index',
	// 	component: () => import('../views/index.vue')
	// },
	{
		path: '/',
		name: 'myIndex',
		component: () => import('../views/mybigScreen/index.vue')
	}
]
const router = new VueRouter({
	routes
})

export default router
