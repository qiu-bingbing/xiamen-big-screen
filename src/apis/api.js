/**
 * 系统应用
 *
 * @author yubaoshan
 * @date 2020/5/26 19:06
 */
import {
	axios
} from '../requestUtils/request'

/**
 * 登录
 *
 * @author yubaoshan
 * @date 2020/5/26 19:06
 */
export function login(parameter) {
	console.log(16, parameter);
	return axios({
		url: '/login',
		method: 'post',
		data: parameter
	})
}

/**
 * 登出
 *
 * @author yubaoshan
 * @date 2020/5/26 19:07
 */
export function logout(parameter) {
	return axios({
		url: '/logout',
		method: 'get',
		params: parameter
	})
}

//获取医院列表/GetCompany
export function GetCompany(parameter) {
	return axios({
		url: '/GetCompany',
		method: 'get',
		params: parameter
	})
}

//当月存入数量
export function GetRecNumber(parameter) {
	return axios({
		url: '/api/TextileLogs/GetRecNumber',
		method: 'get',
		params: parameter
	})
}
//当月领取数量
export function GetOutNumber(parameter) {
	return axios({
		url: '/api/TextileLogs/GetOutNumber',
		method: 'get',
		params: parameter
	})
}
//当月领取人数
export function GetOutPeopleNumber(parameter) {
	return axios({
		url: '/api/TextileLogs/GetOutPeopleNumber',
		method: 'get',
		params: parameter
	})
}
//当月已回收数量
export function GetRecycleNumber(parameter) {
	return axios({
		url: '/api/TextileLogs/GetRecycleNumber',
		method: 'get',
		params: parameter
	})
}
//当日存入数量
export function GetRecNumberOfDay(parameter) {
	return axios({
		url: '/api/TextileLogs/GetRecNumberOfDay',
		method: 'get',
		params: parameter
	})
}
//当日领取数量
export function GetOutNumberOfDay(parameter) {
	return axios({
		url: '/api/TextileLogs/GetOutNumberOfDay',
		method: 'get',
		params: parameter
	})
}
//当日领取人数
export function GetOutPeopleNumberOfDay(parameter) {
	return axios({
		url: '/api/TextileLogs/GetOutPeopleNumberOfDay',
		method: 'get',
		params: parameter
	})
}
//当日已回收数量
export function GetRecycleNumberOfDay(parameter) {
	return axios({
		url: '/api/TextileLogs/GetRecycleNumberOfDay',
		method: 'get',
		params: parameter
	})
}
//进入和离开更衣室
export function GetInAndOutTime(parameter) {
	return axios({
		url: '/api/TextileLogs/GetInAndOutTime',
		method: 'get',
		params: parameter
	})
}
//违规记录
export function GetViolationLog(parameter) {
	return axios({
		url: '/api/TextileLogs/GetViolationLog',
		method: 'get',
		params: parameter
	})
}
//禁用人员名单
export function GetForbiddenPeople(parameter) {
	return axios({
		url: '/api/TextileLogs/GetForbiddenPeople',
		method: 'get',
		params: parameter
	})
}

//柜门总数
export function GetGridNumber(parameter) {
	return axios({
		url: '/api/LockControl/GetGridNumber',
		method: 'get',
		params: parameter
	})
}
//已用柜门
export function GetOccupyGridNumber(parameter) {
	return axios({
		url: '/api/LockControl/GetOccupyGridNumber',
		method: 'get',
		params: parameter
	})
}
//空闲柜门
export function GetNullGridNumber(parameter) {
	return axios({
		url: '/api/LockControl/GetNullGridNumber',
		method: 'get',
		params: parameter
	})
}
///api/TextileLogs/LargeScreenData
//空闲柜门
export function LargeScreenData(parameter) {
	return axios({
		url: '/api/Textile/GetLargerData',
		method: 'get',
		params: parameter
	})
}
